"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const do_mutations_service_1 = require("./do-mutations.service");
const do_mutations_controller_1 = require("./do-mutations.controller");
const typeorm_1 = require("@nestjs/typeorm");
const do_mutations_entity_1 = require("./do-mutations.entity");
let DoMutationsModule = class DoMutationsModule {
};
DoMutationsModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([do_mutations_entity_1.DoMutationsEntity])],
        providers: [do_mutations_service_1.DoMutationsService],
        controllers: [do_mutations_controller_1.DoMutationsController]
    })
], DoMutationsModule);
exports.DoMutationsModule = DoMutationsModule;
//# sourceMappingURL=do-mutations.module.js.map