export declare enum DoStatus {
    New = "New",
    Assigned = "Assigned",
    LoadIn = "LoadIn",
    Started = "Started",
    Mutated = "Mutated",
    Reassigned = "Reassigned",
    Continued = "Continued",
    SignedDeliveryOrder = "SignedDeliveryOrder",
    Finished = "Finished",
    ConfirmedByCustomer = "ConfirmedByCustomer"
}
