import { DoTransactionEntity } from '../do-transactions/do-transactions.entity';
import { DriverEntity } from '../drivers/drivers.entity';
import { LocationsEntity } from '../locations/locations.entity';
import { CustomerEntity } from '../customers/customers.entity';
import { VehicleEntity } from '../vehicles/vehicles.entity';
export declare class DoEntity {
    id: number;
    company: string;
    noDO: string;
    date: Date;
    updatedDate: Date;
    noSO: string;
    customerCode: string;
    customerName: string;
    address1: string;
    address2: string;
    address3: string;
    phone: string;
    noPO: string;
    itemName: string;
    qty: number;
    uom: string;
    weight: number;
    note1: string;
    note2: string;
    spm: string;
    spk: string;
    pickUp: string;
    customerEmail: string;
    collectionEmail: string;
    salesEmail: string;
    deliveryEmail: string;
    status: string;
    lat: number;
    lng: number;
    deviceId: string;
    customerComment: string;
    customer: CustomerEntity;
    transactions: DoTransactionEntity[];
    currentDriver: DriverEntity;
    historyDrivers: DriverEntity[];
    vehicle: VehicleEntity;
    currentTransactionId: number;
    locations: LocationsEntity[];
}
