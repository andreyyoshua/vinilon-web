"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DoStatus;
(function (DoStatus) {
    DoStatus["New"] = "New";
    DoStatus["Assigned"] = "Assigned";
    DoStatus["LoadIn"] = "LoadIn";
    DoStatus["Started"] = "Started";
    DoStatus["Mutated"] = "Mutated";
    DoStatus["Reassigned"] = "Reassigned";
    DoStatus["Continued"] = "Continued";
    DoStatus["SignedDeliveryOrder"] = "SignedDeliveryOrder";
    DoStatus["Finished"] = "Finished";
    DoStatus["ConfirmedByCustomer"] = "ConfirmedByCustomer";
})(DoStatus = exports.DoStatus || (exports.DoStatus = {}));
//# sourceMappingURL=do-status.entity.js.map