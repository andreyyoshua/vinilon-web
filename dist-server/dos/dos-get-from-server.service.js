"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const dos_service_1 = require("./dos.service");
const credentials_service_1 = require("../credentials/credentials.service");
const customers_service_1 = require("../customers/customers.service");
const companies_service_1 = require("../companies/companies.service");
const customers_entity_1 = require("../customers/customers.entity");
const credentials_entity_1 = require("../credentials/credentials.entity");
const dos_entity_1 = require("./dos.entity");
const util_1 = require("util");
const config_1 = require("../config");
let qs = require('qs');
let parseString = require('xml2js').parseString;
let GetDoFromServerService = class GetDoFromServerService {
    constructor(service, credentialService, customerService, companyService, httpService) {
        this.service = service;
        this.credentialService = credentialService;
        this.customerService = customerService;
        this.companyService = companyService;
        this.httpService = httpService;
    }
    getData(companies, currentIndex) {
        const date = new Date();
        const a = date.getDate() * 10;
        const pass = 'P@ssw0rd';
        const b = (date.getMonth() + 1) * 100;
        const rhs = 'R@h4si4';
        const c = (date.getFullYear() - 2000) * 1000;
        const key = `${a}${pass}${b}${rhs}${c}`;
        console.log(key, date);
        const company = companies[currentIndex];
        console.log(company);
        this.httpService.post(`${config_1.config.apiEndpoint}`, qs.stringify({ kunci: key, mcu: company.code }), {
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
            },
        }).subscribe(next => {
            parseString(next.data, (err, res) => {
                if (!res)
                    return;
                if (currentIndex < companies.length - 1) {
                    this.getData(companies, currentIndex + 1);
                }
                const dataSet = res.DataSet;
                if (dataSet == null || dataSet === undefined) {
                    return;
                }
                const diffGram = dataSet['diffgr:diffgram'];
                if (diffGram == null || diffGram === undefined) {
                    return;
                }
                const firstDiffGram = diffGram[0];
                if (firstDiffGram == null || firstDiffGram === undefined) {
                    return;
                }
                const newDataSet = firstDiffGram.NewDataSet;
                if (newDataSet == null || newDataSet === undefined) {
                    return;
                }
                const firstNewDataSet = newDataSet[0];
                if (firstNewDataSet == null || firstNewDataSet === undefined) {
                    return;
                }
                const dn = firstNewDataSet.DN;
                if (dn == null || dn === undefined) {
                    return;
                }
                const datas = dn;
                datas.forEach(data => {
                    const doEntity = this.parseDoEntity(data);
                    const customer = new customers_entity_1.CustomerEntity();
                    customer.code = doEntity.customerCode;
                    customer.name = doEntity.customerName;
                    customer.email = doEntity.customerEmail;
                    const credential = new credentials_entity_1.CredentialEntity();
                    credential.email = doEntity.customerEmail === '' ? doEntity.customerCode : doEntity.customerEmail;
                    credential.password = '1234';
                    this.createOrUpdateCredential(credential, savedCredential => {
                        customer.credential = savedCredential;
                        this.createOrUpdateCustomer(customer, savedCustomer => {
                            doEntity.customer = savedCustomer;
                            this.createOrUpdateDOEntity(doEntity, savedDoEntity => {
                                console.log('Success');
                            });
                        });
                    });
                });
            });
        }, error => {
        });
    }
    parseDoEntity(data) {
        const company = data.COMPANY != null ? util_1.isString(data.COMPANY[0]) ? data.COMPANY[0] : '' : '';
        const noDO = data.NO_DO != null ? util_1.isString(data.NO_DO[0]) ? data.NO_DO[0] : '' : '';
        const tglDO = data.TGL_DO != null ? util_1.isString(data.TGL_DO[0]) ? data.TGL_DO[0] : '' : '';
        const noSO = data.NO_SO != null ? util_1.isString(data.NO_SO[0]) ? data.NO_SO[0] : '' : '';
        const customerCode = data.KODE_CUSTOMER != null ? util_1.isString(data.KODE_CUSTOMER[0]) ? data.KODE_CUSTOMER[0] : '' : '';
        const customerName = data.NAMA_CUSTOMER != null ? util_1.isString(data.NAMA_CUSTOMER[0]) ? data.NAMA_CUSTOMER[0] : '' : '';
        const address1 = data.ADD_1 != null ? util_1.isString(data.ADD_1[0]) ? data.ADD_1[0] : '' : '';
        const address2 = data.ADD_2 != null ? util_1.isString(data.ADD_2[0]) ? data.ADD_2[0] : '' : '';
        const address3 = data.ADD_3 != null ? util_1.isString(data.ADD_3[0]) ? data.ADD_3[0] : '' : '';
        const phone = data.PHONE != null ? util_1.isString(data.PHONE[0]) ? data.PHONE[0] : '' : '';
        const noPO = data.NO_PO != null ? util_1.isString(data.NO_PO[0]) ? data.NO_PO[0] : '' : '';
        const namaBarang = data.NAMA_BARANG != null ? util_1.isString(data.NAMA_BARANG[0]) ? data.NAMA_BARANG[0] : '' : '';
        const qty = data.QTY != null ? util_1.isString(data.QTY[0]) ? data.QTY[0] : '' : '';
        const uom = data.UOM != null ? util_1.isString(data.UOM[0]) ? data.UOM[0] : '' : '';
        const beratBarang = data.BERAT_BARANG != null ? util_1.isString(data.BERAT_BARANG[0]) ? data.BERAT_BARANG[0] : '' : '';
        const notes1 = data.KETERANGAN_1 != null ? util_1.isString(data.KETERANGAN_1[0]) ? data.KETERANGAN_1[0] : '' : '';
        const notes2 = data.KETERANGAN_2 != null ? util_1.isString(data.KETERANGAN_2[0]) ? data.KETERANGAN_2[0] : '' : '';
        const spm = data.SPM != null ? util_1.isString(data.SPM[0]) ? data.SPM[0] : '' : '';
        const emailCollection = data.EMAIL_COLLECTION != null ? util_1.isString(data.EMAIL_COLLECTION[0]) ? data.EMAIL_COLLECTION[0] : '' : '';
        const emailCustomer = data.EMAIL_CUSTOMER != null ? util_1.isString(data.EMAIL_CUSTOMER[0]) ? data.EMAIL_CUSTOMER[0] : '' : '';
        const emailSales = data.EMAIL_SALES != null ? util_1.isString(data.EMAIL_SALES[0]) ? data.EMAIL_SALES[0] : '' : '';
        const emailDelivery = data.EMAIL_DELIVERY != null ? util_1.isString(data.EMAIL_DELIVERY[0]) ? data.EMAIL_DELIVERY[0] : '' : '';
        console.log('Company code', company);
        console.log('No DO', noDO);
        console.log('EMAIL Collection', emailCollection);
        console.log('EMAIL Customer', emailCustomer);
        console.log('EMAIL sales', emailSales);
        const doEntity = new dos_entity_1.DoEntity();
        doEntity.company = company.trim();
        doEntity.noDO = noDO.trim();
        doEntity.date = new Date(tglDO);
        doEntity.noSO = noSO.trim();
        doEntity.customerCode = customerCode.trim();
        doEntity.customerName = customerName.trim();
        doEntity.address1 = address1.trim();
        doEntity.address2 = address2.trim();
        doEntity.address3 = address3.trim();
        doEntity.phone = phone.trim();
        doEntity.noPO = noPO.trim();
        doEntity.itemName = namaBarang.trim();
        doEntity.qty = isNaN(qty) ? 0 : parseInt(qty);
        doEntity.qty = isNaN(doEntity.qty) ? 0 : doEntity.qty;
        doEntity.uom = uom.trim();
        doEntity.weight = isNaN(beratBarang) ? 0 : parseInt(beratBarang);
        doEntity.weight = isNaN(doEntity.weight) ? 0 : doEntity.weight;
        doEntity.note1 = notes1.trim();
        doEntity.note2 = notes2.trim();
        doEntity.spm = spm.trim();
        doEntity.collectionEmail = emailCollection.trim();
        doEntity.customerEmail = emailCustomer.trim();
        doEntity.salesEmail = emailSales.trim();
        doEntity.deliveryEmail = emailDelivery.trim();
        return doEntity;
    }
    createOrUpdateCompany(company, listener) {
        this.companyService.findOne(null, {
            where: {
                code: company.code,
            },
        }).then(savedCompany => {
            if (savedCompany) {
                listener(savedCompany);
            }
            else {
                this.companyService.createOne(company, []).then(saved => {
                    listener(saved);
                }, error => {
                    console.log(error);
                    listener(null);
                });
            }
        });
    }
    createOrUpdateCredential(credential, listener) {
        this.credentialService.findOne(null, {
            where: {
                email: credential.email,
                password: credential.password,
            },
        }).then(savedCredential => {
            if (savedCredential) {
                listener(savedCredential);
            }
            else {
                this.credentialService.createOne(credential, []).then(saved => {
                    listener(saved);
                }, error => {
                    console.log(error);
                    listener(null);
                });
            }
        });
    }
    createOrUpdateDOEntity(doEntity, listener) {
        this.service.findOne(null, {
            where: {
                noDO: doEntity.noDO,
            },
        }).then(savedDo => {
            if (savedDo) {
                listener(savedDo);
            }
            else {
                this.service.createOne(doEntity, []).then(saved => {
                    listener(saved);
                }, error => {
                    console.log(error);
                    listener(null);
                });
            }
        });
    }
    createOrUpdateCustomer(customer, listener) {
        this.customerService.findOne(null, {
            where: {
                code: customer.code,
            },
        }).then(savedCustomer => {
            if (savedCustomer) {
                listener(savedCustomer);
            }
            else {
                this.customerService.createOne(customer, []).then(saved => {
                    listener(saved);
                }, error => {
                    console.log(error);
                    listener(null);
                });
            }
        });
    }
};
GetDoFromServerService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [dos_service_1.DosService,
        credentials_service_1.CredentialsService,
        customers_service_1.CustomersService,
        companies_service_1.CompaniesService,
        common_1.HttpService])
], GetDoFromServerService);
exports.GetDoFromServerService = GetDoFromServerService;
//# sourceMappingURL=dos-get-from-server.service.js.map