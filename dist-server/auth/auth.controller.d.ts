import { HttpStatus } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UsersService } from '../users/users.service';
import { CustomersService } from '../customers/customers.service';
import { DriversService } from '../drivers/drivers.service';
export declare class AuthController {
    private readonly authService;
    private userService;
    private customerService;
    private driverService;
    constructor(authService: AuthService, userService: UsersService, customerService: CustomersService, driverService: DriversService);
    createToken(type: string, body: JwtPayload): Promise<{
        expiresIn: number;
        accessToken: string;
    }>;
    logout(req: any): HttpStatus;
    findAll(req: any): Promise<import("../users/users.entity").UserEntity>;
    getData(req: any, type?: string): Promise<import("../customers/customers.entity").CustomerEntity> | Promise<import("../drivers/drivers.entity").DriverEntity>;
}
