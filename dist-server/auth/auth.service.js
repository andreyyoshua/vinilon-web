"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const credentials_service_1 = require("../credentials/credentials.service");
const user_type_entity_1 = require("../users/user-type.entity");
const drivers_service_1 = require("../drivers/drivers.service");
const users_service_1 = require("../users/users.service");
const customers_service_1 = require("../customers/customers.service");
let AuthService = class AuthService {
    constructor(jwtService, credentialService, driverService, customersService, usersService) {
        this.jwtService = jwtService;
        this.credentialService = credentialService;
        this.driverService = driverService;
        this.customersService = customersService;
        this.usersService = usersService;
    }
    createToken(user, type) {
        return this.login(user, type).then(next => {
            if (next) {
                next.type = type;
                const accessToken = this.jwtService.sign(JSON.parse(JSON.stringify(next)));
                return {
                    expiresIn: 360000,
                    accessToken,
                };
            }
            else {
                throw new common_1.UnauthorizedException();
            }
        });
    }
    validateUser(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.login(payload['credential'], payload['type']).then(next => {
                return next;
            });
        });
    }
    login(user, type) {
        return this.credentialService.findOne({
            where: user,
        }).then((next) => {
            const filter = {
                where: {
                    credential: next,
                },
                relations: ['credential'],
            };
            switch (type) {
                case user_type_entity_1.UserType.Driver:
                    return this.driverService.findOne(filter).then(next => {
                        if (next) {
                            return next;
                        }
                        else {
                            filter.where = {
                                phone: user.email,
                            };
                            return this.driverService.findOne(filter).then(next => {
                                if (next && (next.credential === undefined || next.credential.password !== user.password)) {
                                    throw new common_1.UnauthorizedException();
                                }
                                return next;
                            });
                        }
                    });
                case user_type_entity_1.UserType.ExternalDriver:
                    return this.driverService.findOne(filter);
                case user_type_entity_1.UserType.Customer:
                    return this.customersService.findOne(filter).then(next => {
                        console.log('hahahaha1', next);
                        if (next) {
                            return next;
                        }
                        else {
                            filter.where = {
                                code: user.email
                            };
                            return this.customersService.findOne(filter).then(next => {
                                if (next) {
                                    if (next.credential === undefined || next.credential.password !== user.password) {
                                        throw new common_1.UnauthorizedException();
                                    }
                                    return next;
                                }
                                else {
                                    filter.where = {
                                        email: user.email
                                    };
                                    return this.customersService.findOne(filter).then(next => {
                                        if (next && (next.credential === undefined || next.credential.password !== user.password)) {
                                            throw new common_1.UnauthorizedException();
                                        }
                                        return next;
                                    });
                                }
                            });
                        }
                    });
                default:
                    filter.relations.push('acls');
                    return this.usersService.findOne(filter);
            }
        });
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        credentials_service_1.CredentialsService,
        drivers_service_1.DriversService,
        customers_service_1.CustomersService,
        users_service_1.UsersService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map