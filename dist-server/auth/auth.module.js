"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const passport_1 = require("@nestjs/passport");
const auth_controller_1 = require("./auth.controller");
const auth_service_1 = require("./auth.service");
const jwt_strategy_1 = require("./jwt.strategy");
const credentials_service_1 = require("../credentials/credentials.service");
const users_service_1 = require("../users/users.service");
const drivers_service_1 = require("../drivers/drivers.service");
const customers_service_1 = require("../customers/customers.service");
const typeorm_1 = require("@nestjs/typeorm");
const credentials_entity_1 = require("../credentials/credentials.entity");
const users_entity_1 = require("../users/users.entity");
const drivers_entity_1 = require("../drivers/drivers.entity");
const customers_entity_1 = require("../customers/customers.entity");
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    common_1.Module({
        imports: [
            passport_1.PassportModule.register({ defaultStrategy: 'jwt' }),
            jwt_1.JwtModule.register({
                secretOrPrivateKey: 'secretKey',
                signOptions: {
                    expiresIn: '30d',
                },
            }),
            typeorm_1.TypeOrmModule.forFeature([credentials_entity_1.CredentialEntity, users_entity_1.UserEntity, drivers_entity_1.DriverEntity, customers_entity_1.CustomerEntity])
        ],
        controllers: [auth_controller_1.AuthController],
        providers: [
            jwt_strategy_1.JwtStrategy,
            users_service_1.UsersService,
            credentials_service_1.CredentialsService,
            drivers_service_1.DriversService,
            customers_service_1.CustomersService,
            auth_service_1.AuthService
        ]
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map