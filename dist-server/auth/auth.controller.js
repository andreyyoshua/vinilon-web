"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const auth_service_1 = require("./auth.service");
const users_service_1 = require("../users/users.service");
const customers_service_1 = require("../customers/customers.service");
const drivers_service_1 = require("../drivers/drivers.service");
let AuthController = class AuthController {
    constructor(authService, userService, customerService, driverService) {
        this.authService = authService;
        this.userService = userService;
        this.customerService = customerService;
        this.driverService = driverService;
    }
    createToken(type, body) {
        return this.authService.createToken(body, type);
    }
    logout(req) {
        return common_1.HttpStatus.OK;
    }
    findAll(req) {
        return this.userService.findOne(req.user.id, {
            relations: ['acls', 'company']
        }).then(next => {
            console.log(next);
            return next;
        });
    }
    getData(req, type) {
        if (type) {
            if (type == 'driver') {
                return this.driverService.findOne(req.user.id);
            }
            else if (type == 'customer') {
                return this.customerService.findOne(req.user.id);
            }
        }
        else {
            throw new common_1.UnauthorizedException();
        }
    }
};
__decorate([
    common_1.Post('token'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __param(0, common_1.Query('type')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "createToken", null);
__decorate([
    common_1.Post('token/logout'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "logout", null);
__decorate([
    common_1.Get('me'),
    common_1.UseGuards(passport_1.AuthGuard()),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "findAll", null);
__decorate([
    common_1.Get('data'),
    common_1.UseGuards(passport_1.AuthGuard()),
    __param(0, common_1.Req()), __param(1, common_1.Query('type')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "getData", null);
AuthController = __decorate([
    common_1.Controller('auth'),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        users_service_1.UsersService,
        customers_service_1.CustomersService,
        drivers_service_1.DriversService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map