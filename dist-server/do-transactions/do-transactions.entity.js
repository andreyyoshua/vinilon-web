"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const drivers_entity_1 = require("../drivers/drivers.entity");
const medias_entity_1 = require("../medias/medias.entity");
const dos_entity_1 = require("../dos/dos.entity");
let DoTransactionEntity = class DoTransactionEntity {
    constructor() {
        this.date = new Date();
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], DoTransactionEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoTransactionEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.ManyToOne(type => drivers_entity_1.DriverEntity, object => object.transactions, {
        onDelete: 'SET NULL'
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", drivers_entity_1.DriverEntity)
], DoTransactionEntity.prototype, "assignedDriver", void 0);
__decorate([
    typeorm_1.OneToMany(type => medias_entity_1.MediaEntity, object => object.transaction),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], DoTransactionEntity.prototype, "medias", void 0);
__decorate([
    typeorm_1.ManyToOne(type => dos_entity_1.DoEntity, entity => entity.transactions),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", dos_entity_1.DoEntity)
], DoTransactionEntity.prototype, "do", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Date)
], DoTransactionEntity.prototype, "date", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoTransactionEntity.prototype, "problem", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoTransactionEntity.prototype, "notes", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoTransactionEntity.prototype, "lastPosition", void 0);
__decorate([
    typeorm_1.Column({
        unique: true,
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoTransactionEntity.prototype, "mutationCode", void 0);
DoTransactionEntity = __decorate([
    typeorm_1.Entity()
], DoTransactionEntity);
exports.DoTransactionEntity = DoTransactionEntity;
//# sourceMappingURL=do-transactions.entity.js.map