import { DoTransactionEntity } from './do-transactions.entity';
import { DoTransactionsService } from './do-transactions.service';
import { DosService } from '../dos/dos.service';
import { VehiclesService } from '../vehicles/vehicles.service';
import { BlastEmailService } from '../blast-email/blast-email.service';
export declare class DoTransactionsController {
    service: DoTransactionsService;
    private doService;
    private vehicleService;
    private blastEmailService;
    constructor(service: DoTransactionsService, doService: DosService, vehicleService: VehiclesService, blastEmailService: BlastEmailService);
    createOne(params: any, body: DoTransactionEntity): Promise<void>;
    driverMutation(req: any, doid: string, body: any): Promise<DoTransactionEntity>;
    private sendNotification;
    private sendEmail;
}
