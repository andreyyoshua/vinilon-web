"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const swagger_1 = require("@nestjs/swagger");
const path_1 = require("path");
const app_module_1 = require("./app.module");
const admin = require("firebase-admin");
const config_1 = require("./config");
const http_exception_filter_1 = require("./http-exception.filter");
const user_type_entity_1 = require("./users/user-type.entity");
const users_service_1 = require("./users/users.service");
const users_entity_1 = require("./users/users.entity");
const module_entity_1 = require("./modules/module.entity");
const acls_entity_1 = require("./acls/acls.entity");
const modules_service_1 = require("./modules/modules.service");
const credentials_service_1 = require("./credentials/credentials.service");
const acls_service_1 = require("./acls/acls.service");
var serviceAccount = require("../vinilon-firebase-adminsdk-29nxt-f7be987343.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://vinilon.firebaseio.com"
});
const fs = require('fs');
function createAcls(service) {
    return createIfThereIsNoModules(service).then(createdModules => {
        return createdModules.map(module => {
            const aclEntity = new acls_entity_1.AclsEntity();
            aclEntity.moduleName = module.name;
            aclEntity.manager = false;
            aclEntity.operator = true;
            return aclEntity;
        });
    });
}
function createIfThereIsNoModules(service) {
    return service.find().then(modules => {
        if (modules.length === 0) {
            return service.createMany({ bulk: createModules() }, []);
        }
        return modules;
    });
}
function createModules() {
    const moduleNames = ['Customer', 'Driver', 'Kendaraan', 'Kendala', 'Perusahaan', 'User', 'Delivery Order',
        'Status Delivery Order', 'Mutasi', 'Laporan', 'Blast Email'];
    return moduleNames.map(moduleName => {
        const module = new module_entity_1.ModuleEntity();
        module.name = moduleName;
        return module;
    });
}
function createIfThereIsNoSuperAdmin(service, credentialService, aclsService, moduleService) {
    service.findOne({
        level: user_type_entity_1.UserLevel.SuperAdmin,
    }, { relations: ['acls'] }).then(found => {
        if (!found) {
            credentialService.createOne({
                id: 0,
                email: 'super@admin.com',
                password: '1234',
            }, []).then(credential => {
                const superAdmin = new users_entity_1.UserEntity();
                superAdmin.credential = credential;
                superAdmin.name = 'Super Admin';
                superAdmin.level = user_type_entity_1.UserLevel.SuperAdmin;
                service.createOne(superAdmin, []).then(savedSuperAdmin => {
                    createAcls(moduleService).then(acls => {
                        acls.forEach(acl => {
                            acl.user = savedSuperAdmin;
                            acl.manager = true;
                            acl.operator = true;
                        });
                        aclsService.createMany({ bulk: acls }).then(next => {
                            console.log("Superadmin Created");
                        });
                    });
                });
            });
        }
        else if (found.acls && found.acls.length == 0) {
            createAcls(moduleService).then(acls => {
                acls.forEach(acl => {
                    acl.user = found;
                    acl.manager = true;
                    acl.operator = true;
                });
                aclsService.createMany({ bulk: acls }).then(next => {
                    console.log("Superadmin Created");
                }, error => {
                    console.log(error);
                });
            });
        }
        else {
            credentialService.findOne({ email: 'super@admin.com' }).then(credential => {
                credential.password = config_1.config.superadminPassword;
                credentialService.updateOne(credential).then(next => {
                    console.log('Superadmin updated');
                });
            });
        }
    });
}
function assignedAclToUser(userService, moduleService, aclService) {
    userService.find({ relations: ['acls'] }).then(users => {
        moduleService.find().then(modules => {
            users.filter(user => user.level !== user_type_entity_1.UserLevel.SuperAdmin).forEach(user => {
                if (modules.length !== user.acls.length) {
                    createAcls(moduleService).then(acls => {
                        acls.forEach(acl => acl.user = user);
                        aclService.createMany({ bulk: acls }).then(next => {
                            console.log("User updated");
                        });
                    });
                }
            });
        });
    });
}
function bootstrap() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = yield core_1.NestFactory.create(app_module_1.AppModule);
        app.setGlobalPrefix("/v1/api");
        app.useGlobalFilters(new http_exception_filter_1.AllExceptionsFilter());
        app.use((req, res, next) => {
            if (req.url.indexOf('%23') != -1) {
                res.redirect(req.url.replace('%23', '#'));
                return;
            }
            else if (req.url.indexOf('/pages') != -1) {
                res.redirect('/#' + req.url);
                return;
            }
            next();
        });
        app.useStaticAssets(path_1.join(__dirname, '..', 'dist'));
        app.useStaticAssets(path_1.join(__dirname, '..', 'upload'), {
            prefix: '/upload',
        });
        const options = new swagger_1.DocumentBuilder()
            .build();
        const document = swagger_1.SwaggerModule.createDocument(app, options);
        document.basePath = "/v1/api";
        swagger_1.SwaggerModule.setup('/v1/api/doc', app, document);
        try {
            let data = JSON.stringify(document);
            fs.writeFileSync('../frontend/swagger.json', data);
        }
        catch (error) {
            console.log(error);
        }
        const moduleService = app.get(modules_service_1.ModulesService);
        const userService = app.get(users_service_1.UsersService);
        const credentialService = app.get(credentials_service_1.CredentialsService);
        const aclService = app.get(acls_service_1.AclsService);
        createIfThereIsNoModules(moduleService).then(next => {
            createIfThereIsNoSuperAdmin(userService, credentialService, aclService, moduleService);
        });
        yield app.listen(config_1.config.port);
    });
}
bootstrap();
//# sourceMappingURL=main.js.map