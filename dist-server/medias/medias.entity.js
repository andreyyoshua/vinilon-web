"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const do_transactions_entity_1 = require("../do-transactions/do-transactions.entity");
const blast_email_entity_1 = require("../blast-email/blast-email.entity");
let MediaEntity = class MediaEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], MediaEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], MediaEntity.prototype, "url", void 0);
__decorate([
    typeorm_1.ManyToOne(type => do_transactions_entity_1.DoTransactionEntity, transaction => transaction.medias),
    __metadata("design:type", do_transactions_entity_1.DoTransactionEntity)
], MediaEntity.prototype, "transaction", void 0);
__decorate([
    typeorm_1.ManyToOne(type => blast_email_entity_1.BlastEmailEntity, transaction => transaction.medias),
    __metadata("design:type", blast_email_entity_1.BlastEmailEntity)
], MediaEntity.prototype, "email", void 0);
MediaEntity = __decorate([
    typeorm_1.Entity()
], MediaEntity);
exports.MediaEntity = MediaEntity;
//# sourceMappingURL=medias.entity.js.map