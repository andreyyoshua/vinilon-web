"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const credentials_entity_1 = require("./credentials.entity");
const crud_1 = require("@nestjsx/crud");
const credentials_service_1 = require("./credentials.service");
const users_service_1 = require("../users/users.service");
let CredentialsController = class CredentialsController {
    constructor(service, userService) {
        this.service = service;
        this.userService = userService;
    }
};
CredentialsController = __decorate([
    crud_1.Crud(credentials_entity_1.CredentialEntity),
    crud_1.Feature('Credential'),
    common_1.Controller('credentials'),
    __metadata("design:paramtypes", [credentials_service_1.CredentialsService, users_service_1.UsersService])
], CredentialsController);
exports.CredentialsController = CredentialsController;
//# sourceMappingURL=credentials.controller.js.map