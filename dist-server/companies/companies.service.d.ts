import { CompanyEntity } from './companies.entity';
import { RepositoryService } from '@nestjsx/crud/typeorm';
export declare class CompaniesService extends RepositoryService<CompanyEntity> {
    constructor(repo: any);
}
