"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const do_mutations_entity_1 = require("../do-mutations/do-mutations.entity");
let ProblemEntity = class ProblemEntity {
};
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ProblemEntity.prototype, "id", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], ProblemEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.OneToMany(type => do_mutations_entity_1.DoMutationsEntity, object => object.problem),
    __metadata("design:type", do_mutations_entity_1.DoMutationsEntity)
], ProblemEntity.prototype, "mutation", void 0);
ProblemEntity = __decorate([
    typeorm_1.Entity()
], ProblemEntity);
exports.ProblemEntity = ProblemEntity;
//# sourceMappingURL=problems.entity.js.map