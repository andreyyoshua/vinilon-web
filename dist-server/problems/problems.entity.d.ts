import { DoMutationsEntity } from '../do-mutations/do-mutations.entity';
export declare class ProblemEntity {
    id: number;
    name: string;
    mutation: DoMutationsEntity;
}
