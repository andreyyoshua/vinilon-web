import { CompanyEntity } from '../companies/companies.entity';
import { CredentialEntity } from '../credentials/credentials.entity';
import { AclsEntity } from '../acls/acls.entity';
export declare class UserEntity {
    id: number;
    name: string;
    address: string;
    phone: string;
    email: string;
    company?: CompanyEntity;
    department: string;
    token: string;
    isDelete: boolean;
    level: string;
    credential: CredentialEntity;
    acls: AclsEntity[];
}
