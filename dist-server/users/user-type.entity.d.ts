export declare enum UserType {
    Driver = "Driver",
    ExternalDriver = "ExternalDriver",
    Customer = "Customer"
}
export declare enum UserLevel {
    SuperAdmin = "SuperAdmin",
    Admin = "Admin",
    Regular = "Regular"
}
