"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserType;
(function (UserType) {
    UserType["Driver"] = "Driver";
    UserType["ExternalDriver"] = "ExternalDriver";
    UserType["Customer"] = "Customer";
})(UserType = exports.UserType || (exports.UserType = {}));
var UserLevel;
(function (UserLevel) {
    UserLevel["SuperAdmin"] = "SuperAdmin";
    UserLevel["Admin"] = "Admin";
    UserLevel["Regular"] = "Regular";
})(UserLevel = exports.UserLevel || (exports.UserLevel = {}));
//# sourceMappingURL=user-type.entity.js.map