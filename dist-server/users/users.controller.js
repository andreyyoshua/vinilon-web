"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const users_entity_1 = require("./users.entity");
const users_service_1 = require("./users.service");
const credentials_service_1 = require("../credentials/credentials.service");
const acl_service_1 = require("../acl/acl.service");
const acls_service_1 = require("../acls/acls.service");
const modules_service_1 = require("../modules/modules.service");
const acls_entity_1 = require("../acls/acls.entity");
const user_type_entity_1 = require("./user-type.entity");
const passport_1 = require("@nestjs/passport");
let UsersController = class UsersController {
    constructor(service, credentialService, aclsService, modulesService) {
        this.service = service;
        this.credentialService = credentialService;
        this.aclsService = aclsService;
        this.modulesService = modulesService;
    }
    get base() {
        return this;
    }
    getMany(req, parsedQuery, parsedOptions) {
        parsedQuery.filter.push({
            field: 'level',
            operator: 'ne',
            value: user_type_entity_1.UserLevel.SuperAdmin
        });
        parsedQuery.or.push({
            field: 'level',
            operator: 'isnull',
        });
        return this.base.getManyBase(parsedQuery, parsedOptions);
    }
    createOne(params, body) {
        body.credential.email = body.email;
        return this.credentialService.createOne(body.credential, [])
            .then(next => {
            body.credential = next;
            return this.service.createOne(body, []).then(user => {
                return this.createAcls().then(acls => {
                    acls.forEach(acl => acl.user = user);
                    this.aclsService.createMany({ bulk: acls }).then(next => {
                        return user;
                    });
                });
            });
        });
    }
    createAcls() {
        return this.modulesService.find().then(createdModules => {
            return createdModules.map(module => {
                const aclEntity = new acls_entity_1.AclsEntity();
                aclEntity.moduleName = module.name;
                aclEntity.manager = false;
                aclEntity.operator = false;
                return aclEntity;
            });
        });
    }
};
__decorate([
    crud_1.Override('getManyBase'),
    __param(0, common_1.Req()), __param(1, crud_1.ParsedQuery()), __param(2, crud_1.ParsedOptions()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, crud_1.RestfulParamsDto, Object]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "getMany", null);
__decorate([
    crud_1.Override('createOneBase'),
    __param(0, crud_1.ParsedParams()),
    __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, users_entity_1.UserEntity]),
    __metadata("design:returntype", void 0)
], UsersController.prototype, "createOne", null);
UsersController = __decorate([
    common_1.UseGuards(passport_1.AuthGuard(), acl_service_1.ACLGuard),
    crud_1.Feature('Users'),
    crud_1.Crud(users_entity_1.UserEntity, {
        options: {
            join: {
                company: {
                    allow: [],
                },
                credential: {
                    allow: [],
                },
                acls: {
                    allow: [],
                },
            },
        },
    }),
    common_1.Controller('users'),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        credentials_service_1.CredentialsService,
        acls_service_1.AclsService,
        modules_service_1.ModulesService])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=users.controller.js.map