import { RestfulParamsDto, CrudController, CrudOptions } from '@nestjsx/crud';
import { UserEntity } from './users.entity';
import { UsersService } from './users.service';
import { CredentialsService } from '../credentials/credentials.service';
import { AclsService } from '../acls/acls.service';
import { ModulesService } from '../modules/modules.service';
import { AclsEntity } from '../acls/acls.entity';
export declare class UsersController {
    service: UsersService;
    credentialService: CredentialsService;
    private aclsService;
    private modulesService;
    constructor(service: UsersService, credentialService: CredentialsService, aclsService: AclsService, modulesService: ModulesService);
    readonly base: CrudController<UsersService, UserEntity>;
    getMany(req: any, parsedQuery: RestfulParamsDto, parsedOptions: CrudOptions): Promise<UserEntity[] | import("@nestjsx/crud").GetManyDefaultResponse<UserEntity>>;
    createOne(params: any, body: UserEntity): Promise<void>;
    createAcls(): Promise<AclsEntity[]>;
}
