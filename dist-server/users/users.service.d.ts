import { UserEntity } from './users.entity';
import { RepositoryService } from '@nestjsx/crud/typeorm';
export declare class UsersService extends RepositoryService<UserEntity> {
    constructor(repo: any);
}
