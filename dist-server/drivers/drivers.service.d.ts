import { DriverEntity } from './drivers.entity';
import { RepositoryService } from '@nestjsx/crud/typeorm';
export declare class DriversService extends RepositoryService<DriverEntity> {
    constructor(repo: any);
}
