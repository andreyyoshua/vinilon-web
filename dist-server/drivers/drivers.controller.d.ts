import { DriverEntity } from './drivers.entity';
import { CrudController } from '@nestjsx/crud';
import { DriversService } from './drivers.service';
import { CredentialsService } from '../credentials/credentials.service';
export declare class DriversController {
    service: DriversService;
    credentialService: CredentialsService;
    constructor(service: DriversService, credentialService: CredentialsService);
    readonly base: CrudController<DriversService, DriverEntity>;
    createOne(params: any, body: DriverEntity): Promise<DriverEntity>;
}
