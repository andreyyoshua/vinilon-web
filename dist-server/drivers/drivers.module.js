"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const drivers_entity_1 = require("./drivers.entity");
const drivers_service_1 = require("./drivers.service");
const drivers_controller_1 = require("./drivers.controller");
const typeorm_1 = require("@nestjs/typeorm");
const credentials_entity_1 = require("../credentials/credentials.entity");
const credentials_service_1 = require("../credentials/credentials.service");
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
let DriversModule = class DriversModule {
};
DriversModule = __decorate([
    common_1.Module({
        imports: [
            passport_1.PassportModule.register({ defaultStrategy: 'jwt' }),
            jwt_1.JwtModule.register({
                secretOrPrivateKey: 'secretKey',
                signOptions: {
                    expiresIn: 3600,
                },
            }),
            typeorm_1.TypeOrmModule.forFeature([drivers_entity_1.DriverEntity, credentials_entity_1.CredentialEntity])
        ],
        providers: [drivers_service_1.DriversService, credentials_service_1.CredentialsService],
        controllers: [drivers_controller_1.DriversController]
    })
], DriversModule);
exports.DriversModule = DriversModule;
//# sourceMappingURL=drivers.module.js.map