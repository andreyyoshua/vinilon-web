"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const locations_entity_1 = require("./locations.entity");
const locations_service_1 = require("./locations.service");
const dos_service_1 = require("../dos/dos.service");
let LocationsController = class LocationsController {
    constructor(service, doService) {
        this.service = service;
        this.doService = doService;
    }
    createOne(params, body) {
        return this.doService.findOne({
            id: body['doId']
        }).then(deliveryOrder => {
            body.do = deliveryOrder;
            return this.service.createOne(body, []);
        });
    }
};
__decorate([
    crud_1.Override(),
    __param(0, crud_1.ParsedParams()),
    __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, locations_entity_1.LocationsEntity]),
    __metadata("design:returntype", void 0)
], LocationsController.prototype, "createOne", null);
LocationsController = __decorate([
    crud_1.Crud(locations_entity_1.LocationsEntity),
    crud_1.Feature('Locations'),
    common_1.Controller('locations'),
    __metadata("design:paramtypes", [locations_service_1.LocationsService, dos_service_1.DosService])
], LocationsController);
exports.LocationsController = LocationsController;
//# sourceMappingURL=locations.controller.js.map