import { DriverEntity } from '../drivers/drivers.entity';
import { DoEntity } from '../dos/dos.entity';
export declare class VehicleEntity {
    id: number;
    policeNumber: string;
    category: string;
    type: string;
    caroseryDimension: string;
    truckDimension: string;
    notes: string;
    isDelete: boolean;
    driver: DriverEntity;
    dos: DoEntity[];
}
