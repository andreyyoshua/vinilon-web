"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const drivers_entity_1 = require("../drivers/drivers.entity");
const dos_entity_1 = require("../dos/dos.entity");
let VehicleEntity = class VehicleEntity {
};
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], VehicleEntity.prototype, "id", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], VehicleEntity.prototype, "policeNumber", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], VehicleEntity.prototype, "category", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], VehicleEntity.prototype, "type", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], VehicleEntity.prototype, "caroseryDimension", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], VehicleEntity.prototype, "truckDimension", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], VehicleEntity.prototype, "notes", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true, default: false }),
    __metadata("design:type", Boolean)
], VehicleEntity.prototype, "isDelete", void 0);
__decorate([
    typeorm_1.OneToOne(type => drivers_entity_1.DriverEntity, {
        onDelete: 'CASCADE',
    }),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", drivers_entity_1.DriverEntity)
], VehicleEntity.prototype, "driver", void 0);
__decorate([
    typeorm_1.OneToMany(type => dos_entity_1.DoEntity, object => object.vehicle),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], VehicleEntity.prototype, "dos", void 0);
VehicleEntity = __decorate([
    typeorm_1.Entity()
], VehicleEntity);
exports.VehicleEntity = VehicleEntity;
//# sourceMappingURL=vehicles.entity.js.map