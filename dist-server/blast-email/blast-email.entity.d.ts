import { MediaEntity } from '../medias/medias.entity';
export declare class BlastEmailEntity {
    id: number;
    actionCode: string;
    action: string;
    sales: boolean;
    customer: boolean;
    collection: boolean;
    delivery: boolean;
    template: string;
    subject: string;
    medias?: MediaEntity[];
}
