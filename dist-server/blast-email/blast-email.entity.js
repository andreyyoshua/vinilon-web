"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const medias_entity_1 = require("../medias/medias.entity");
let BlastEmailEntity = class BlastEmailEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], BlastEmailEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], BlastEmailEntity.prototype, "actionCode", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], BlastEmailEntity.prototype, "action", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Boolean)
], BlastEmailEntity.prototype, "sales", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Boolean)
], BlastEmailEntity.prototype, "customer", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Boolean)
], BlastEmailEntity.prototype, "collection", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Boolean)
], BlastEmailEntity.prototype, "delivery", void 0);
__decorate([
    typeorm_1.Column({
        type: 'text'
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], BlastEmailEntity.prototype, "template", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], BlastEmailEntity.prototype, "subject", void 0);
__decorate([
    typeorm_1.OneToMany(type => medias_entity_1.MediaEntity, object => object.email),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], BlastEmailEntity.prototype, "medias", void 0);
BlastEmailEntity = __decorate([
    typeorm_1.Entity()
], BlastEmailEntity);
exports.BlastEmailEntity = BlastEmailEntity;
//# sourceMappingURL=blast-email.entity.js.map