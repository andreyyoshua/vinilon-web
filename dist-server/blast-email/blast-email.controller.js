"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const blast_email_entity_1 = require("./blast-email.entity");
const blast_email_service_1 = require("./blast-email.service");
let BlastEmailController = class BlastEmailController {
    constructor(service) {
        this.service = service;
        this.createIfThereIsNo('Mengantar Barang', 'send');
        this.createIfThereIsNo('Sampai di Tujuan', 'finish');
        this.createIfThereIsNo('Mutasi Barang', 'mutate');
        this.createIfThereIsNo('Barang sudah di Terima', 'confirmed');
    }
    createIfThereIsNo(action, actionCode) {
        this.service.findOne({
            where: {
                action,
            },
        }).then(found => {
            if (!found) {
                this.service.createOne({
                    id: 0,
                    action,
                    actionCode,
                    sales: true,
                    collection: true,
                    delivery: true,
                    template: '',
                    subject: '',
                    customer: true,
                }, []).then(created => {
                    if (created) {
                        console.log(action + ' is created');
                    }
                });
            }
            else {
                found.action = action;
                found.actionCode = actionCode;
                this.service.updateOne(found).then(updated => {
                    if (updated) {
                        console.log(updated + 'is updated');
                    }
                });
            }
        });
    }
};
BlastEmailController = __decorate([
    crud_1.Crud(blast_email_entity_1.BlastEmailEntity, {
        options: {
            join: {
                medias: {
                    allow: [],
                },
            },
        },
    }),
    crud_1.Feature('Blast-Email'),
    common_1.Controller('blast-email'),
    __metadata("design:paramtypes", [blast_email_service_1.BlastEmailService])
], BlastEmailController);
exports.BlastEmailController = BlastEmailController;
//# sourceMappingURL=blast-email.controller.js.map