import { RepositoryService } from '@nestjsx/crud/typeorm';
import { BlastEmailEntity } from './blast-email.entity';
import { DosService } from '../dos/dos.service';
export declare class BlastEmailService extends RepositoryService<BlastEmailEntity> {
    private doService;
    constructor(repo: any, doService: DosService);
    sendMailToDo(doId: number, actionCode: string): Promise<void>;
    private sendMail;
}
