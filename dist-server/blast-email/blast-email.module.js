"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const blast_email_service_1 = require("./blast-email.service");
const blast_email_controller_1 = require("./blast-email.controller");
const typeorm_1 = require("@nestjs/typeorm");
const blast_email_entity_1 = require("./blast-email.entity");
const dos_entity_1 = require("../dos/dos.entity");
const dos_service_1 = require("../dos/dos.service");
let BlastEmailModule = class BlastEmailModule {
};
BlastEmailModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([blast_email_entity_1.BlastEmailEntity, dos_entity_1.DoEntity])],
        providers: [blast_email_service_1.BlastEmailService, dos_service_1.DosService],
        controllers: [blast_email_controller_1.BlastEmailController],
    })
], BlastEmailModule);
exports.BlastEmailModule = BlastEmailModule;
//# sourceMappingURL=blast-email.module.js.map