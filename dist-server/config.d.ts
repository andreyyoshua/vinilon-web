export declare const config: {
    apiEndpoint: string;
    port: number;
    jobsPort: number;
    jobsDuration: number;
    smtpHost: string;
    smtpPort: number;
    smtpUsername: string;
    smtpPassword: string;
    superadminPassword: string;
};
