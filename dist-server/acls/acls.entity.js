"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const users_entity_1 = require("../users/users.entity");
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
let AclsEntity = class AclsEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], AclsEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], AclsEntity.prototype, "moduleName", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Boolean)
], AclsEntity.prototype, "manager", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Boolean)
], AclsEntity.prototype, "operator", void 0);
__decorate([
    typeorm_1.ManyToOne(type => users_entity_1.UserEntity, transaction => transaction.acls, { onDelete: 'CASCADE' }),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", users_entity_1.UserEntity)
], AclsEntity.prototype, "user", void 0);
AclsEntity = __decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    typeorm_1.Entity()
], AclsEntity);
exports.AclsEntity = AclsEntity;
//# sourceMappingURL=acls.entity.js.map