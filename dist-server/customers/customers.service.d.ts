import { CustomerEntity } from './customers.entity';
import { RepositoryService } from '@nestjsx/crud/typeorm';
export declare class CustomersService extends RepositoryService<CustomerEntity> {
    constructor(repo: any);
}
